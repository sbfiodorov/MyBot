import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SendPhoto;

import java.util.*;
import java.util.stream.Collectors;

public class TelegramBotApplication extends TelegramBot
{
    private static BotState botState = BotState.START;

    @lombok.Builder
    public TelegramBotApplication(String botToken)
    {
        super(botToken);
    }
    public void run()
    {
        this.setUpdatesListener
        (
          updates ->
        {
            updates.forEach(this::process);
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        }
        );
    }
    private void process(Update update)
    {
        Message message = update.message();
        if (update.message() != null)
        {
            String text = message.text();
            ParseData pd = new ParseData();
            pd.parseContinents();
            pd.parseCountry();
            pd.parseWaters();
            setButtons(message.chat().id(), text, new String[][]
                    {
                            {"Start", "Help"},
                            {"1", "2", "3"},
                            {"4", "5", "6"},
                            {"7", "8", "9"}
                    });

            switch (botState)
            {
                case FIRST_WORD :
                {
                    List<DataWaters> dw = pd.parseWaters();
                    for(DataWaters dataWaters : dw)
                    {
                        if(text.charAt(0) == dataWaters.getNameWater().charAt(0))
                        {
                            String a = dataWaters.toString();
                            SendPhoto sendPhoto = new SendPhoto(message.chat().id(), dataWaters.getPhoto());
                            this.execute(sendPhoto);
                            SendMessage response = new SendMessage(message.chat().id(), a);
                            this.execute(response);
                        }
                    }
                    botState = BotState.START;
                    break;
                }

                case NAME_WATER :
                {
                    List<DataWaters> dw = pd.parseWaters();
                    for(DataWaters dataWaters : dw)
                    {
                        if(text.equals(dataWaters.getNameWater()))
                        {
                            String a = dataWaters.toString();
                            SendPhoto sendPhoto = new SendPhoto(message.chat().id(), dataWaters.getPhoto());
                            this.execute(sendPhoto);
                            SendMessage response = new SendMessage(message.chat().id(), a);
                            this.execute(response);
                        }
                    }
                    botState = BotState.START;
                    break;
                }

                case NAME_COUNTRY :
                {
                    for(Map.Entry<DataWaters, DataCountries> entry : ParseData.map4.entrySet())
                    {
                        if(Objects.equals(text, entry.getValue().getCountry()))
                        {
                            String str = entry.getKey().toString();
                            SendPhoto sendPhoto = new SendPhoto(message.chat().id(), entry.getKey().getPhoto());
                            this.execute(sendPhoto);
                            SendMessage response3 = new SendMessage(message.chat().id(), str);
                            this.execute(response3);
                        }
                    }
                    botState = BotState.START;
                    break;
                }

                case NAME_CONTINENT :
                {
                    for(Map.Entry<DataWaters, DataCountries> entry : ParseData.map4.entrySet())
                    {
                        if(Objects.equals(text, entry.getValue().getContinent().getContinentsName()))
                        {
                            String str = entry.getKey().toString();
                            SendPhoto sendPhoto = new SendPhoto(message.chat().id(), entry.getKey().getPhoto());
                            this.execute(sendPhoto);
                            SendMessage response4 = new SendMessage(message.chat().id(), str);
                            this.execute(response4);
                        }
                    }
                    botState = BotState.START;
                    break;
                }

                case SUM_OF_COUNTRY :
                {
                    int lenght = 0;
                    int area = 0;
                    int depth = 0;
                    String resultLength = null;
                    String resultArea = null;
                    String resultDepth = null;
                    for (Map.Entry<DataWaters, DataCountries> entry : ParseData.map4.entrySet())
                    {
                        if (Objects.equals(text, entry.getValue().getCountry()))
                        {
                            if (Objects.equals(entry.getKey().getType(), "река"))
                            {
                                lenght = lenght + entry.getKey().getArea();
                                resultLength = String.valueOf(lenght);
                            }
                        }
                        if (Objects.equals(text, entry.getValue().getCountry()))
                        {
                            if (!Objects.equals(entry.getKey().getType(), "река"))
                            {
                                area = area + entry.getKey().getArea();
                                resultArea = String.valueOf(area);
                            }
                        }
                        if (Objects.equals(text, entry.getValue().getCountry()))
                        {
                            depth = depth + entry.getKey().getDepth();
                            resultDepth = String.valueOf(depth);
                        }
                    }
                    SendMessage resA = new SendMessage(message.chat().id(), "Длина рек : " + resultLength);
                    this.execute(resA);
                    SendMessage resB = new SendMessage(message.chat().id(), "Площадь озёр и морей : " + resultArea);
                    this.execute(resB);
                    SendMessage resD = new SendMessage(message.chat().id(), "Глубина : " + resultDepth);
                    this.execute(resD);
                    botState = BotState.START;
                    break;
                }

                case SUM_OF_CONTINENT :
                {
                    int lenght = 0;
                    int area = 0;
                    int depth = 0;
                    String resultLength = null;
                    String resultArea = null;
                    String resultDepth = null;
                    for (Map.Entry<DataWaters, DataCountries> entry : ParseData.map4.entrySet())
                    {
                        if (Objects.equals(text, entry.getValue().getContinent().getContinentsName()))
                        {
                            if (Objects.equals(entry.getKey().getType(), "река"))
                            {
                                lenght = lenght + entry.getKey().getArea();
                                resultLength = String.valueOf(lenght);
                            }
                        }
                        if (Objects.equals(text, entry.getValue().getContinent().getContinentsName()))
                        {
                            if (!Objects.equals(entry.getKey().getType(), "река"))
                            {
                                area = area + entry.getKey().getArea();
                                resultArea = String.valueOf(area);
                            }
                        }
                        if (Objects.equals(text, entry.getValue().getContinent().getContinentsName()))
                        {
                            depth = depth + entry.getKey().getDepth();
                            resultDepth = String.valueOf(depth);
                        }
                    }
                    SendMessage resA = new SendMessage(message.chat().id(), "Длина рек : " + resultLength);
                    this.execute(resA);
                    SendMessage resB = new SendMessage(message.chat().id(), "Площадь озёр и морей : " + resultArea);
                    this.execute(resB);
                    SendMessage resD = new SendMessage(message.chat().id(), "Глубина : " + resultDepth);
                    this.execute(resD);
                    botState = BotState.START;
                    break;
                }

                case START :
                {
                    switch (text)
                    {
                        case "Start" :
                        {
                            SendMessage text2 = new SendMessage(message.chat().id(), text());
                            this.execute(text2);
                            break;
                        }

                        case "Help" :
                        {
                            SendMessage text2 = new SendMessage(message.chat().id(), "Для тех, кто в танке : \n\n" +
                                    "1) Вводить водоёмы, страны и континенты надо одним словом, и, естественно, с большой буквы\n" +
                                    "2) Вводите водоёмы, страны и континенты только те, которые присуствуют в пунктах - 1,2,3");
                            this.execute(text2);
                            break;
                        }

                        case "1" :
                        {
                            String continents = ParseData.list1.stream().collect(Collectors.joining("\n", "", ""));
                            SendMessage response = new SendMessage(message.chat().id(), "Континенты : \n\n" +  continents);
                            this.execute(response);
                            break;
                        }

                        case "2" :
                        {
                            String countries = ParseData.list2.stream().collect(Collectors.joining("\n", "", ""));
                            SendMessage response = new SendMessage(message.chat().id(),"Страны : \n\n" +   countries);
                            this.execute(response);
                            break;
                        }

                        case "3" :
                        {
                            List<DataWaters> dw = pd.parseWaters();
                            for(DataWaters dataWaters : dw)
                            {
                                String a = dataWaters.toString();
                                SendPhoto sendPhoto = new SendPhoto(message.chat().id(), dataWaters.getPhoto());
                                this.execute(sendPhoto);
                                SendMessage response = new SendMessage(message.chat().id(), a);
                                this.execute(response);
                            }
                            break;
                        }

                        case "4" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите первую букву : ");
                            this.execute(response);
                            botState = BotState.FIRST_WORD;
                            break;
                        }

                        case "5" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите название водоёма : ");
                            this.execute(response);
                            botState = BotState.NAME_WATER;
                            break;
                        }

                        case "6" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите название страны : ");
                            this.execute(response);
                            botState = BotState.NAME_COUNTRY;
                            break;
                        }

                        case "7" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите название континента : ");
                            this.execute(response);
                            botState = BotState.NAME_CONTINENT;
                            break;
                        }

                        case "8" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите название страны : ");
                            this.execute(response);
                            botState = BotState.SUM_OF_COUNTRY;
                            break;
                        }

                        case "9" :
                        {
                            SendMessage response = new SendMessage(message.chat().id(), "Введите название континента : ");
                            this.execute(response);
                            botState = BotState.SUM_OF_CONTINENT;
                            break;
                        }
                    }
                }
            }
        }
    }
    private void setButtons(Long chat_id, String str, String[][] strings)
    {
        SendMessage message = new SendMessage(chat_id, str)
                .replyMarkup(new ReplyKeyboardMarkup(strings).resizeKeyboard(true));
        this.execute(message);
    }
    public String text()
    {
        return "Данный бот выдаёт водоёмы по их странам и континентам.\n" +
        "\nВыберите действие : " +
        "\n1 - Печать имеющихся континентов" +
        "\n2 - Печать имеющихся стран" +
        "\n3 - Печать имеющихся водоёмов" +
        "\n4 - Печать водоёмов по первой букве" +
        "\n5 - печать водоёма по имени" +
        "\n6 - печать водоёмов по стране" +
        "\n7 - печать водоёмов по континенту" +
        "\n8 - печать суммарного значения по стране" +
        "\n9 - печать суммарного значения по континенту";
    }
}