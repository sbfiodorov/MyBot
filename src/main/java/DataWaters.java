import java.util.Objects;

public class DataWaters
{
    String nameWater;
    String type;
    int area;
    int depth;
    String swimmingPool;
    DataCountries country;
    String photo;
    public DataWaters(String nameWater, String type, int area, int depth, String swimmingPool, DataCountries country, String photo)
    {
        this.nameWater = nameWater;
        this.type = type;
        this.area = area;
        this.depth = depth;
        this.swimmingPool = swimmingPool;
        this.country = country;
        this.photo = photo;
    }

    public String getNameWater()
    {
        return nameWater;
    }

    public void setNameWater(String nameWater)
    {
        this.nameWater = nameWater;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getArea()
    {
        return area;
    }

    public void setArea(int area)
    {
        this.area = area;
    }

    public int getDepth()
    {
        return depth;
    }

    public void setDepth(int depth)
    {
        this.depth = depth;
    }

    public String getSwimmingPool()
    {
        return swimmingPool;
    }

    public void setSwimmingPool(String swimmingPool)
    {
        this.swimmingPool = swimmingPool;
    }

    public DataCountries getCountry()
    {
        return country;
    }

    public void setCountry(DataCountries country)
    {
        this.country = country;
    }

    public String getPhoto()
    {
        return photo;
    }

    public void setPhoto(String photo)
    {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataWaters that = (DataWaters) o;
        return area == that.area && depth == that.depth && Objects.equals(nameWater, that.nameWater) && Objects.equals(type, that.type) && Objects.equals(swimmingPool, that.swimmingPool) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(nameWater, type, area, depth, swimmingPool, country);
    }

    @Override
    public String toString()
    {
        return "Название : " + nameWater +
                ", тип : " + type +
                ", площадь : " + area + "м2" +
                ", глубина : " + depth + "м" +
                ", бассейн : " + swimmingPool +
                ", страна : " + country + " континент : " + country.getContinent() + ";\n";
    }
}