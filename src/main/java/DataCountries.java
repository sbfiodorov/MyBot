import java.util.Objects;
public class DataCountries
{
    String country;
    DataContinents continent;

    public DataCountries(String country, DataContinents continents)
    {
        this.country = country;
        this.continent = continents;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public DataContinents getContinent()
    {
        return continent;
    }

    public void setContinent(DataContinents continent)
    {
        this.continent = continent;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCountries that = (DataCountries) o;
        return Objects.equals(country, that.country) && Objects.equals(continent, that.continent);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(country, continent);
    }

    @Override
    public String toString()
    {
        return country;
    }
}
