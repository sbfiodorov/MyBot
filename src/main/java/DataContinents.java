import java.util.Objects;

public class DataContinents
{
    String continentsName;

    public DataContinents(String continentsName)
    {
        this.continentsName = continentsName;
    }

    public String getContinentsName()
    {
        return continentsName;
    }

    public void setContinentsName(String continentsName)
    {
        this.continentsName = continentsName;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataContinents that = (DataContinents) o;
        return Objects.equals(continentsName, that.continentsName);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(continentsName);
    }

    @Override
    public String toString()
    {
        return continentsName;
    }
}
